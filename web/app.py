from flask import Flask, jsonify, request
from flask_restful import Api, Resource
from pymongo import MongoClient
import bcrypt

app=Flask(__name__)
api= Api(app)

client=MongoClient("mongodb://db:27017")
db=client.SentencesDatabase
users=db["users"]

class Register(Resource):
    def post(self):
        postedData=request.get_json()
        #get user logs
        username=postedData["username"]
        password=postedData["password"]
        #crypt users password
        hashed_password= bcrypt.hashpw(password.encode('utf8') , bcrypt.gensalt())
        #insert new user into collection
        users.insert({
            "Username":username,
            "Password":hashed_password,
            "Sentence":"",
            "Tokens":6
        })
        #return status
        retJson={
            "status": 200,
            "message": "you registred successfully"
        }

        return jsonify(retJson)

def verify_password(Username, password):
    hashed_password=users.find({
        'Username':Username
    })[0]['Password']
    if bcrypt.hashpw(password.encode('utf8'),hashed_password)==hashed_password:
        return True
    else:
        return False
def countTokens(username):
    tokens=users.find({
        'Username':username
    })[0]["Tokens"]
    return tokens      

class Store(Resource):
    def post(self):
        #get the posted data
        postedData=request.get_json()
        #get user logs
        username=postedData["username"]
        password=postedData["password"]
        sentence=postedData["sentence"]
        #check user password
        Iscorrect_password= verify_password(username, password)
        
        if not Iscorrect_password:
            retJson={
                'status':302,
                'message':'wrong password'
            }
            return jsonify(retJson)

        num_tokens=countTokens(username)
        if num_tokens<=0:
            retJson={
                'status':301,
                'msg':'not enough tokens'
            }
            return jsonify(retJson)

        users.update({
            "Username":username
        },{
            '$set':{
                'Sentence':sentence,
                'Tokens':num_tokens-1
                }
        })

        retJson={
            'status': 200,
            'msg ':'sentence storage successfully'
        }
        return jsonify(retJson)

class Get(Resource):
    def get(self):

        postedData=request.get_json()
        username=postedData["username"]
        password=postedData["password"]

        Iscorrect_password= verify_password(username, password)
        
        if not Iscorrect_password:
            retJson={
                'status':302,
                'message':'wrong password'
            }
            return jsonify(retJson)

        num_tokens=countTokens(username)
        if num_tokens<=0:
            retJson={
                'status':301,
                'msg':'not enough tokens'
            }
            return jsonify(retJson)

        users.update({
            "Username":username
        },{
            '$set':{
                'Tokens':num_tokens-1
                }
        })

        sentence=users.find({
            "Username":username
        })[0]["Sentence"]

        retJson={
            'status':200,
            "sentence":sentence
        }
        return jsonify(retJson)

api.add_resource(Register, '/register')
api.add_resource(Store, '/store')
api.add_resource(Get, '/get')

if __name__=="__main__":
    app.run(host='0.0.0.0')   